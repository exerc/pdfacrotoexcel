package de.firen;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelWriter {
	
	private static int startRow;
	private static int startColm; 
	private static int catCount;
	private static int tempRowNr;
	public static int catRow = -1;
	private static boolean start = true;
	private static boolean catFound = false;

	public static void write(List<PDField> fields, String filepath, String outputXlsx)
    {
        System.out.println("Start writing process..");
        try
        {	
        	// Specify excel file to work with
            FileInputStream file = new FileInputStream(new File(filepath));
 
            // Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);
 
            // Get desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
           
            // Count categories for indexing the cells necessary
            countCategories(fields);

            // Iterate through PDF fields
            for (PDField field : fields) { 
            	
            	// Save field names and values of the PDF-Acroform
            	String category = field.getFullyQualifiedName();
            	String fieldAsString = field.getValueAsString();
            	
            	// Set counter and boolean for the cell iterating process
            	int counter = catCount;
            	catFound = false;
            	Iterator<Row> rIterator = sheet.iterator();
        	    
            	// Search for given category by iterating through rows and cells
            	while (rIterator.hasNext() && counter >= 0 && !catFound) {	
            		Row r;
            		if (catRow == -1) {
            			r = rIterator.next();
                       	System.out.println("Searching for category in new row..");
            		// Row index is already set in previous cycles
            		} else {
            			r = sheet.getRow(catRow);
            		}
                   	Iterator<Cell> cIterator = r.cellIterator();
                   	
                   	// Search cells in row r for given category and save index for later use (write cycle)
                    while (cIterator.hasNext() && counter >= 0 && !catFound) {
                       	Cell c = cIterator.next();
                        String cValue = c.getStringCellValue();
                       	counter--;
                       	System.out.println("Checking cell entry for category: " + category);
                        System.out.println("Cell entry: \"" + cValue + "\" in cell: " + c.getColumnIndex());
                        if (cValue.equals(category))  {
                        	startColm = c.getColumnIndex();
                        	
                        	catRow = c.getRowIndex();
                        	
                        	catFound = true;
                           	System.out.println("Cell matches category: Category found!");
                           	
                        	// Search for end of table by ++ to row number,
                        	// if row is null or empty -> create new row ONCE and save index for write cycle
                        	if (start) {
                        		tempRowNr = (catRow + 1);
                        		while (start) {
                        			System.out.println("Checking for previous stored data (end of table)");
	                        		if (rowIsEmpty(c, sheet)) {
	                	        		startRow = tempRowNr;
	                        			sheet.createRow(startRow);
	                        			start = false;
	                	        		System.out.println("Setting start row: " + startRow);
	                        		} else {
	                        			tempRowNr++;
	                        			System.out.println("checking next row..");
	                        		}
                        		}
                        	}
                        	
                        	// Write into given cell
                        	fillCell (sheet, fieldAsString);
                        	System.out.println("Iterating over next category");
                        }
                        if (!catFound) {
                        	System.out.println("No match.. Checking next cell");
                        }
                    }
            	}
            }
        	System.out.println("Process finished!");
            
            // Try to write output into Excel file
            try {	
            	OutputStream fileOut = new FileOutputStream(outputXlsx);
                workbook.write(fileOut);
            } catch (Exception e) {
            	e.printStackTrace();
            }
         
            // Close workbook and file input reader
            workbook.close();
            file.close();
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
    }   

	public static boolean rowIsEmpty(Cell cell, XSSFSheet sheet) {
		boolean cellEmpty = true;
		Row r = sheet.getRow(tempRowNr);
		
		// if row is not null cycle through cells and check if cell is blank
			for (int i = 0; i <= catCount;) {
				int cellCount = cell.getColumnIndex() + i++;	
				
				System.out.println("Row index: " + tempRowNr + " Column index: " + cellCount);
				if (r != null) {
					Cell tempCell = r.getCell(cellCount);
					
					System.out.println("Cell is not empty!");
					
					//Check if cell is blank with dataformatter
				    DataFormatter dataFormatter = new DataFormatter();
				    if(dataFormatter.formatCellValue(tempCell).trim().length() > 0) {
				    	return false;			    	
				    }
				}
		    }
		return cellEmpty;
	}

	public static void countCategories(List<PDField> fields) {
		System.out.println("Counting categories to write..");
		for (PDField field : fields) {
			catCount++;
			System.out.println("Nr. of categories: " + catCount);
		}
	}

	public static void fillCell (XSSFSheet sheet, String fieldAsString) {	
		Row r = sheet.getRow(startRow);
		Cell c = r.createCell(startColm);
		
		c.setCellValue(fieldAsString);
		System.out.println("Writing successfull with: " + fieldAsString);				
	}
}

