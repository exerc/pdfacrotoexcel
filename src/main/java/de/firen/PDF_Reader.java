package de.firen;

import java.io.File;
import java.util.List;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.interactive.form.*;

public class PDF_Reader {

	public static List<PDField> listFields(String pdf) throws Exception {
		File file = new File(pdf);
		PDDocument doc = PDDocument.load(file);
		PDDocumentCatalog catalog = doc.getDocumentCatalog();
        PDAcroForm form = catalog.getAcroForm();
        List<PDField> fields = form.getFields();
        
        return fields;
	}	
}