package de.firen;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class ExecWindow {
	
	public String pdfPath = null;
	public String excelPath = null;
	public String outputXlsx = null;
	
	public ExecWindow() {
		
		// Excel file path and output file path may be the same!
		JTextField pdf = new JTextField();
		JTextField excel = new JTextField();
		JTextField output = new JTextField();
		Object[] content = {"PDF file path:", pdf, "Excel file path", excel, "Output file path", output};
		JOptionPane pane = new JOptionPane(content, JOptionPane.PLAIN_MESSAGE, 
				JOptionPane.OK_CANCEL_OPTION);
		
		pane.createDialog(null, "PDF to Excel").setVisible(true);
		pdfPath = pdf.getText();
		excelPath = excel.getText();
		outputXlsx = output.getText();
	}
}
