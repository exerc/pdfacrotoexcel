package de.firen;

import java.util.List;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import javax.swing.*;

public class Main {
	
	private static boolean running = true;
	private static String pdf;
	private static String excel;
	private static String outputXlsx;
	
	public static void main(String[] args) throws Exception {
		ExecWindow window = new ExecWindow();
		
		while(running) {
			if(window.pdfPath != null && window.excelPath != null) {
				pdf = window.pdfPath;
				excel = window.excelPath;
				outputXlsx = window.outputXlsx;
				running = false;
				JOptionPane.getRootFrame().dispose();
			}
		}
		
		// Load pdf file and extract AcroForms
		List<PDField> pdfield = PDF_Reader.listFields(pdf);	
		
		// Write AcroForm fields in pdf file
		ExcelWriter.write(pdfield, excel, outputXlsx);
	}
}
