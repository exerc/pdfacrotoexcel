# PDF AcroForm Reader to Excel

<p>
Simple tool to read AcroForms of a PDF file and save the content according to the given AcroForm field name into an Excel file.  
</p>
<b>How to use</b>
<p>You need a prepared Excel file with the categories to be read out. The category name must be the same as the AcroForm field name. Unwanted categories may be left out.<br>
<br>
The program gives you three text fields to fill out:<br>
<ul>
  <li>PDF file path: (insert file path of your PDF file.)</li>
  <li>Excel file path: (insert file path of the prepared Excel file)</li>
  <li>Output file path: (insert file path to store the newly generated Excel file)</li>
</ul>
</p>
<b>Important note:</b> Excel file path and Output file path may be the same in order to update the file with new content
